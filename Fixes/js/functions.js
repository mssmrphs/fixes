jQuery(document).ready(function($){

    // skip to a certain page
    var $_GET = getQueryParams(document.location.search);
    if ($_GET['skip'] == 'prelander') { skipPrelander(); }


    // on click fuctions
    $('object of click').click(function() {
        goStepVersion();
    });


    // question-page functions
    $('.question-button').click(function() {
        if ($(this).parent().attr('class') == 'last') {
            $('.class').hide(); // set class to display none
            $('.class').show(); // set class to display block
            $("body").removeClass("class"); // remove class from body
            $("body").addClass("class"); // add class to body
			$(this).parent().find('.class').hide(); // trigger this class in only in THIS parent
        } else {
            $(this).parent().fadeOut();
            $(this).parent().next().fadeIn();
        }
    });
	
	// make the button stay active
	$('.optbutton').click(function() {
        $('.optbutton').not(this).removeClass('active').html(function() {
            return;
        });
        $(this).addClass('active');
    });
	
	$('.button').click(function() {
        $('.first-page').hide();
    });

	// set var
    $('object of click').click(function() {
        var colorImg = $('class').attr('src').split('/')[6]; // take variable from part of url
        var color = $(this).attr("rel"); // rel attr of 'object of click'
        $('.color').attr('src', $('.color').attr('src').replace(colorImg, color+'.png')); // replace var1 with an image of var2

        $('.active').not(this).removeClass('active'); // remove active class from everything that is not 'object of click'
        $(this).addClass('active'); // add class active to 'object of click'
    });
	
	    // ASCII Symbols
        var auml = String.fromCharCode(228);
        var uuml = String.fromCharCode(252);

        // Use rel attr to replace text of header
        $('.choice').click(function() {
            var relation = $(this).attr("rel");
            if(relation == 'choice-1') {
                $('header h1').text('This is choice 1');
            } else if (relation == 'choice-2') {
                $('header h1').text('This is choice 2');
            }
        });


    // set alert on click function
    $('.version-options .join-button').click(function() {
        var selectColor = $('.the-color .active').length;
        if(!selectColor) { alert('Bitte w'+auml+'hlen Sie die Farbe Ihrer Wahl aus!'); return false; }
        var selectWeels = $('.the-weels .active2').length;
        if(!selectWeels) { alert('Bitte w'+auml+'hlen Sie die Felgen Ihrer Wahl aus!'); return false; }
    });

		
	// Replace text with HTML
	var h1Text = "<h1>Thank you<br /><span>for your opinion</span></h1>";
	$('.class h1').replaceWith(h1Text);
		
		
	// Replace text with PHP Variable
	jQuery(function($){
        window.CAMPAIGN_LEAD_HEADER_MOB = "<?php echo html_entity_decode(CAMPAIGN_LEAD_HEADER_MOB, ENT_QUOTES, 'UTF-8'); ?>";
        window.CAMPAIGN_LEAD_HEADER = "<?php echo html_entity_decode(CAMPAIGN_LEAD_HEADER, ENT_QUOTES, 'UTF-8'); ?>";
        window.CAMPAIGN_THANKYOU_1 = "<?php echo html_entity_decode(CAMPAIGN_THANKYOU_1, ENT_QUOTES, 'UTF-8'); ?>";
        window.CAMPAIGN_THANKYOU_2 = "<?php echo html_entity_decode(CAMPAIGN_THANKYOU_2, ENT_QUOTES, 'UTF-8'); ?>";

        window.CAMPAIGN_CHECK_1 = "<?php echo html_entity_decode(CAMPAIGN_CHECK_1, ENT_QUOTES, 'UTF-8'); ?>";
        window.CAMPAIGN_CHECK_2 = "<?php echo html_entity_decode(CAMPAIGN_CHECK_2, ENT_QUOTES, 'UTF-8'); ?>";
        window.CAMPAIGN_CHECK_3 = "<?php echo html_entity_decode(CAMPAIGN_CHECK_3, ENT_QUOTES, 'UTF-8'); ?>";
    });
	
	// Simple countdown in seconds
	var count=100;
	var counter=setInterval(timer, 1000);
	function timer() {
		count=count-1;
		if (count <= 0) {
			clearInterval(counter);
			document.getElementById("crazy-counter").innerHTML="X";
			return;
        }
        document.getElementById("crazy-counter").innerHTML=count;
	}

	
	// scroll to top
	$('html, body').animate({scrollTop: $('.scroll-to-class-here').offset().top}, 0);

	// add function to certain class
	$(this).hasClass('your-class')
	
	// find certain class
	$(this).find('.your-class')
	
	// testing function:
	console.log('whatever you want console log to show');
	
	// time-out and delay functions
	setTimeout(function(){ $('.your-class').fadeIn(2000); }, 3000);
	$('.your-class').delay(1600).fadeOut(300);
	
	// replace (part of) image src
	var img = $(this).find("img");
    img.prop("src", img.prop("src").replace("old", "new"));
	
	$('.img-class').attr('src', $('.img-class').attr('src').replace('old', 'new'));
	
	// unbind the click event once you trigger it:
	('.class').one('click', function(e) { }
	
	// remove class from sibling
	.siblings().removeClass('active')

	.length // looks if there is any 'size', if there is, it exists, if not, the length = false so it doesnt exist
	.length() // counts characters
	
	// check limit
	var limit = x;
	if($(".your-class").length >= limit) { }
	
	
	// first selector --> other selectors: https://api.jquery.com/eq-selector/
	$('.your-class:first');
	
	// check if var is empty
	if (typeof VAR_NAME !== 'undefined') {	}
});




// go step version
function goStepQuestions() {
    $('.questions, .prize-header').show();
    $('.choices').hide();
    $("body").removeClass("choice-page");
    $("body").addClass("question-page");
    $('.prize-header .left .title').text('Geben Sie bitte Ihre Daten zur Teilnahme ein');
}



// countdown to set time                   
<script type="text/javascript">
    function toppersCounter() {
        var concert = new Date("May 13, 2016 20:31:07");
        var now = new Date();
        var timeDiff = concert.getTime() - now.getTime();

        var seconds = Math.floor(timeDiff / 1000);
        var minutes = Math.floor(seconds / 60);
        var hours = Math.floor(minutes / 60);
        var days = Math.floor(hours / 24);
        hours %= 24;
        minutes %= 60;
        seconds %= 60;

        days = ("00" + days).substr(-2);
        hours = ("00" + hours).substr(-2);
        minutes = ("00" + minutes).substr(-2);
        seconds = ("00" + seconds).substr(-2);

        if (timeDiff <= 0) {
            // clearTimeout(timer);
            $('.counter h4').hide();
            $('.counter h5').show();

            days = "00";
            hours = "00";
            minutes = "00";
            seconds = "00";
        }

        document.getElementById("daysBox").innerHTML = days;
        document.getElementById("hoursBox").innerHTML = hours;
        document.getElementById("minsBox").innerHTML = minutes;
        document.getElementById("secsBox").innerHTML = seconds;
        var timer = setTimeout('toppersCounter()',1000);
    }
</script>

<div id="daysBox" class="counter-box"></div>
<div id="hoursBox" class="counter-box"></div>
<div id="minsBox" class="counter-box"></div>
<div id="secsBox" class="counter-box"></div>
<script type="text/javascript">toppersCounter();</script>
