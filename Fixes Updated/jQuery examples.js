    // include js + jQuery libraries
    // <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    // <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    // <script type="text/javascript" src="js/main.js"></script>
	
	// nice urls:
	// http://jqueryui.com/effect/ (effects)
	// https://api.jquery.com/eq-selector/ (selectors)

jQuery(document).ready(function($){
	
	//======================================================================
	// GENERAL INFO AND OPTIONS
	//======================================================================

   // retrieve a certain parameter from the url
   var $_GET = getQueryParams(document.location.search);
   $_GET['your-parameter']
   
   // setting variables
   var yourVar = "something";
   $otherVar = "something";
   
   
	// several actions
	$('.class').hide(); // add display: none to .class
	$('.class').show(); // add display: block to .class
    $('.class').removeClass('remove-me'); // remove .remove-me from .class
    $('.class').addClass('add-me'); // add .add-me to .class
	$('.class').fadeIn(); // fade .class in (has to be invisible to take effect)
	$('.class').fadeOut(); // fade .class out (has to be visible to take effect)
	$('.class').html(); // retrieve the html inside .class
	$('.class').effect('bounce', 1000); // add an effect
	$('.class').index(); // gets the index number of that class
	
	
	// various functions
	$('.class').length // checks if there is any 'size', if there is, it exists, if not, the length = false so it doesnt exist
	// number of (child)elements --> .children().length
	$('.class').length() // counts characters
	$('.class').one('click', function(e) { } ); // unbind 'click' after first time
	$('.class').mouseenter(function() { }); // do something on hover
	$('.class').click(function() { }); // do something on click
	
	
	// various if/else statements
	if (typeof neverDeclared !== 'undefined') {	} // check if var is defined
	if($(".class").length >= limit) { } // check limit (set var with limit first!)
	

	// various replace methodes
	$('.class').text('this is the new text'); // don't use html
	$('.class h1').replaceWith('<div>new header</div>');
	
	
	// several ways to trigger elements
	$('.class').parent(); // trigger the parent
	$('.class').siblings(); // trigger the siblings of class
	$('.class').prev(); // trigger the previous sibling
	$('.class').next(); // trigger the next sibling
	$('.class').find('.find-me'); // find elements with class 'find-me' inside .class
	$('.class').not('.me'); // trigger all .class but not the one with class me
	$('.class').attr('text') // trigger .class with a certain attribute
	$('.class').hasClass('find-me'); // triggers all elements with 'find me' class inside .class
	
	
	// testing
	// to see if your function or variable is working, you can use console.log:
	console.log('whatever you want console log to show');
	
	
	
	//======================================================================
	// EXAMPLES
	//======================================================================
	
	// close navigation when clicked outside
	$(document).click(function (e) {
		if(!$(e.target).closest('.nav-class').length) {
			$('.is-toggled').removeClass('is-toggled');
		}
	});
	
	
	// make the button stay active
	$('.button').click(function() {
        $('.button').not(this).removeClass('active').html(function() {
            return;
        });
        $(this).addClass('active');
    });
	
	
	
	// replace image on click
    $('object of click').click(function() {
        var var1 = $('.class').attr('src').split('/')[6]; // take variable from part of url
        var var2 = $(this).attr("rel"); // rel attr of 'object of click'
        $('.class2').attr('src', $('.class2').attr('src').replace(var1, var2+'.png')); // replace var1 with an image of var2
    });
	
	
	// replace (part of) image src
	var img = $('.class').find("img");
    img.prop("src", img.prop("src").replace("old", "new"));
	
	$('.img-class').attr('src', $('.img-class').attr('src').replace('old', 'new'));
	
	
	// scroll to top
	$('html, body').animate({scrollTop: $('.class').offset().top}, 0);
	
	
	// time-out and delay functions
	setTimeout(function(){ $('.class').fadeIn(2000); }, 3000);
	$('.class').delay(1600).fadeOut(300);
	
	
	// Simple countdown in seconds
	var count=100;
	var counter=setInterval(timer, 1000);
	function timer() {
		count=count-1;
		if (count <= 0) {
			clearInterval(counter);
			document.getElementById("crazy-counter").innerHTML="X";
			return;
        }
        document.getElementById("crazy-counter").innerHTML=count;
	}
	
	
	// Replace text on click based on rel
    $('.class').click(function() {
        var relation = $(this).attr("rel");
		
        if(relation == 'choice-1') {
			$('header h1').text('This is choice 1');
		} else if (relation == 'choice-2') {
			$('header h1').text('This is choice 2');
        }
		
    });
	
	
	// set ASCII Symbols
    var auml = String.fromCharCode(228);
    var uuml = String.fromCharCode(252);
	
	
	// set alert on click function
	// stay on page when alert is given
    $('.class').click(function() {
        var selectColor = $('.the-color .active').length;
        if(!selectColor) { alert('Color not set!'); return false; }
    });
	
	
	// make element fade out and next element fade in
    $('.class').click(function() {
        $(this).parent().fadeOut();
        $(this).parent().next().fadeIn();
    });
	
});





// countdown to set time                   
<script type="text/javascript">
    function countDown() {
        var endDate = new Date("December 21, 2012 20:20:20");
        var now = new Date();
        var timeDiff = endDate.getTime() - now.getTime();

        var seconds = Math.floor(timeDiff / 1000);
        var minutes = Math.floor(seconds / 60);
        var hours = Math.floor(minutes / 60);
        var days = Math.floor(hours / 24);
        hours %= 24;
        minutes %= 60;
        seconds %= 60;

        days = ("00" + days).substr(-2);
        hours = ("00" + hours).substr(-2);
        minutes = ("00" + minutes).substr(-2);
        seconds = ("00" + seconds).substr(-2);

        if (timeDiff <= 0) {
            // clearTimeout(timer);
            days = "00";
            hours = "00";
            minutes = "00";
            seconds = "00";
        }

        document.getElementById("daysBox").innerHTML = days;
        document.getElementById("hoursBox").innerHTML = hours;
        document.getElementById("minsBox").innerHTML = minutes;
        document.getElementById("secsBox").innerHTML = seconds;
        var timer = setTimeout('toppersCounter()',1000);
    }
	
	// check total file input before uploading
	// ad onclick="AcceptableFileUpload()" to button, put outside 'document ready function!'
	function AcceptableFileUpload() {
        var totalsize = 0;

        $('input:file').each(function () {
            if ($(this).val().length > 0) {
                totalsize = totalsize + $(this)[0].files[0].size;
            }
        });
        if (totalsize > 1073741) {
            alert("File size limit exceeded");
            return false;
        }
        return true;
    }
	
</script>

<div id="daysBox" class="counter-box"></div>
<div id="hoursBox" class="counter-box"></div>
<div id="minsBox" class="counter-box"></div>
<div id="secsBox" class="counter-box"></div>
<script type="text/javascript">countDown();</script>




