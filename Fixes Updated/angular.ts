@Component({
	selector: 'my-app',
	template: 	`<div>
				<h1>{{title}}</h1>

				// [] and {{}} both do the same! -->
				<input type="text" value={{title}} />
				<input type="text" [value]="title" />

				// banana in a box notation:
				<input type="text" [(ngModel)]="title" />

				// when you add an eventlister use ()
				// it's 'click' not 'onclick' etc
				// event effectief aanroepen (use a template variable (#) --> this gives you the html markup, so if you want to have the value of the html-element (for exaple), you have to add .value)
				// instead of # you can use 'ref-'

				<input type="text" #game placeholder="add your game" />
				<button (click)="addGame(game.value)">Add Game</button>

				// if you want to show the list only when there are games, add 'ngIf'
				// length can only be 0 or positive (not negative)
				<ul *ngIf="games && games.length">
					// for each statement
					<li *ngFor="let game of games">
						{{game}}
						// if game has different values you can use 'game.title' or 'game.year'
					</li>
				</ul>

				
				</div>`
})

export class AppComponent {
	title: string = 'Hello!'; // you can add value directly
	title2: string; // you can add 'public', but it is public by default

	// create a list to add your games too
	public games: Array<string> = new Array<string>();

	public addGame(game: string) : void {
		this.games.push(game);
	}
}

// {{ xxx }} --> one way methode (read only)
// [(ngModel)] --> banana in a box, two way methode (eventlister behind it, watches onchange)

// difference attribute and property
	// attribute --> checks html
	// property --> checks state of javascript
	// attribute gives back 'checked=checked', property gives back 'true'
	// in angular some attributes are not available as property
		// for example 'colspan', you should use --> [attr.colspan]="1+1"