// some scroll functions for the thumbrail
function sliderScrollRight(container) {
	var container = $(container);
	var leftPos = container.scrollLeft();
	container.animate({ scrollLeft: leftPos + 200 }, 500);
}

function sliderScrollLeft(container) {
	var container = $(container);
	var leftPos = container.scrollLeft();
	container.animate({ scrollLeft: leftPos - 200 }, 500);
}

function sliderScrollUp(container) {
	var container = $(container);
	var topPos = container.scrollTop();
	container.animate({ scrollTop: topPos - 400 }, 500);

	setTimeout(function() { 
		setThumbRailButtonStatus();
	}, 500);
}

function sliderScrollDown(container) {
	var container = $(container);
	var topPos = container.scrollTop();
	container.animate({ scrollTop: topPos + 400 }, 500);

	setTimeout(function() { 
		setThumbRailButtonStatus();
	}, 500);
}

// fills the modal window on page load
// adds all 'slider images' to the modal window
function activateSlider() {
	$(modal).hide();

	// add all slider images on the page to the 'thumbnailrail'
	$('.js-sliderimg').each(function (index, value) {
		var thumbUrl = $(this).attr('src');
		$('.js-thumbnails').append("<img class='c-thumbslider__img' src='" + thumbUrl + "' alt='' />");
	});

	// show first image
	var firstImg = $('.js-thumbnails').find('img:first');
	var firstImgUrl = firstImg.attr('src');
	firstImg.addClass('is-active');

	$('.js-slider__content').html("<img class='c-imgslider__img js-currentimg' src='" + firstImgUrl + "' alt='' />");

	// deactivate prev & up button
	$('.o-slider__left, .o-slider__up').addClass('is-disabled');
}

// opens the modal window when a thumbnail is clicked
$('.js-sliderimg').click(function(){
	var currentImg = $('.js-currentimg');
	var currentImgUrl = currentImg.attr('src');
	
	var selectedSliderImg = $(this);
	var newImgUrl = selectedSliderImg.attr('src');
	
	currentImg.attr('src', currentImg.attr('src').replace(currentImgUrl, newImgUrl));

	var thumb = $('.js-thumbnails').find('img[src="' + newImgUrl + '"]');

	$('.js-thumbnails').find('.is-active').removeClass('is-active');
	thumb.addClass('is-active');
	
	$(modal).show();

	scrollToActiveImgInSlider();
	// wait till scroll is finished
	setTimeout(function() { 
		setSliderButtonStatus(thumb);
	}, 500);
});

// click function navigate buttons on the left (main image: left/right): shows next or previous image
function sliderSelect(dir) {
	var currentImg = $('.js-currentimg');
	var thumbnails = $('.js-thumbnails');

	var currentImgUrl = currentImg.attr('src');
	
	var currentThumb = thumbnails.find('img[src="' + currentImgUrl + '"]');

	if (dir == "left" || dir == "previous" || dir == "prev") {
		var newImg = currentThumb.prev();
	}

	else if (dir == "right" || dir == "next") {
		var newImg = currentThumb.next();
	}

	else {
		console.log("define dir (direction as string) as: left, previous, prev, right, or next!");
		return false;
	}

	if (newImg !== undefined && newImg.length) {
		$('.js-thumbnails').find('.is-active').removeClass('is-active');
		newImg.addClass('is-active');
		var newImgUrl = newImg.attr('src');
	
		currentImg.attr('src', currentImg.attr('src').replace(currentImgUrl, newImgUrl));

		scrollToActiveImgInSlider(); 
		// wait till scroll is finished
		setTimeout(function() { 
			setThumbRailButtonStatus();
			setSliderButtonStatus(newImg);
		}, 500);
	}
}

// click function for thumbnails inside the modal window
// because the thumbnail didn't exist yet on page load, we use document onclick to make it accessable.
$(document).on('click', '.js-thumbnails img', function() {
	var currentImg = $('.js-currentimg');
	var currentImgUrl = currentImg.attr('src');
	
	var selectedSliderImg = $(this);
	var newImgUrl = selectedSliderImg.attr('src');

	$('.js-thumbnails').find('.is-active').removeClass('is-active');
	selectedSliderImg.addClass('is-active');
	
	currentImg.attr('src', currentImg.attr('src').replace(currentImgUrl, newImgUrl));

	scrollToActiveImgInSlider();
	setSliderButtonStatus(selectedSliderImg);
	// wait till scroll is finished
	setTimeout(function() {
		setThumbRailButtonStatus();
	}, 500);
});

// (de)activates slider buttons for the main image (left/right)
function setSliderButtonStatus(newImg) {
	var prevAvailable = false;
	var nextAvailable = false;

	if (newImg.next().length) nextAvailable = true;
	if (newImg.prev().length) prevAvailable = true;

	(prevAvailable)
		? $('.o-slider__left').removeClass('is-disabled')
		: $('.o-slider__left').addClass('is-disabled');

	(nextAvailable)
		? $('.o-slider__right').removeClass('is-disabled')
		: $('.o-slider__right').addClass('is-disabled');
}

// (de)activates slider buttons for the thumbrail (up/down)
function setThumbRailButtonStatus() {
	var container = $('.c-thumbslider__content');
	var containerHeight = container.innerHeight();
	var containerScrolled = container.scrollTop();
	var totalHeight = 0;
	
	$('.c-thumbslider__img').each(function (index, value) {
		totalHeight += this.height;
	});

	if (totalHeight <= containerHeight) {
		// we see everything, we don't need to scroll
		$('.o-slider__down, .o-slider__up').addClass('is-disabled');
		return false;
	}

	// we don't see everything, determine scroll position
	if (containerScrolled >= (totalHeight - containerHeight - 2)) {
		// bottom reached
		$('.o-slider__down').addClass('is-disabled');
		$('.o-slider__up').removeClass('is-disabled');
	} else if (containerScrolled == 0) {
		// top reached
		$('.o-slider__up').addClass('is-disabled');
		$('.o-slider__down').removeClass('is-disabled');
	} else {
		$('.o-slider__down, .o-slider__up').removeClass('is-disabled');
	}
}


function scrollToActiveImgInSlider() {
	var isActive = '.js-thumbnails .is-active';
	var container = '.c-thumbslider__content';
	goTo(isActive, 150, 500, container);
}


/*
.o-slider__buttons
	.c-modal__button.o-slider__arrow.o-slider__left(onclick="sliderSelect('prev')")
	.c-modal__button.o-slider__arrow.o-slider__right(onclick="sliderSelect('next')")

.o-slider.o-slider__img.c-imgslider.js-slider__content

.o-slider.o-slider__thumbs.c-thumbslider
	.c-thumbslider__arrow
		.c-modal__button.o-slider__arrow.o-slider__up(onclick="sliderScrollUp('.c-thumbslider__content')")
	.c-thumbslider__content.js-thumbnails
	.c-thumbslider__arrow
		.c-modal__button.o-slider__arrow.o-slider__down(onclick="sliderScrollDown('.c-thumbslider__content')")
*/