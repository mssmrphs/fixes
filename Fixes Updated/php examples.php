<?php // open tag


//======================================================================
// COMMENTS
//====================================================================== 

//-----------------------------------------------------
// Some examples / ideas
//-----------------------------------------------------

/* Title Here Notice the First Letters are Capitalized */

# Option 1
# Option 2
# Option 3

/*
* This is a detailed explanation
* of something that should require
* several paragraphs of information.
*/

// This is a single line quote.




//======================================================================
// VARIABLES & CONSTANTS
//======================================================================

// set variables
$yourVar = 'this is a variable';

// define constants
define('YOUR_CONSTANT', 'this is a constant');

// a variable you can overwrite at any given point
// a constant you can define just once
//// if you try to overwrite a constant, it will give an error message
//// constants are by rule written in capitals


// to add variables / constants to a normal string use a dot:
$oldVar = 'your oldVar';
$newVar = 'this is your string +' . $oldVar;
// output would be: 'this is your string + your oldVar'



// check if a variable is set / constant is defined
isset($variable);
defined('CONSTANT');

//// for example
if(isset($variable) && $variable != '') { echo "Hi!"; }

//// or:
if(defined('CONSTANT') && CONSTANT == 'hello') { echo "Hello World!"; }


//======================================================================
// ECHO
//======================================================================

// echo prints down your text on the page
echo $newVar;

//// will show on the page the text 'this is your string + your oldVar'
echo 'this is your string';

//// if you want to echo a variable and a string, use double quotes:
echo "Show me $oldVar";

//// if you use single quotes, use the . to connect the string and the var:
echo 'show me ' . $oldVar;


//======================================================================
// IF ELSE STATEMENTS
//======================================================================


// if else statements
if('your condition') { /* what it should do */ }
if('your condition'): /* what it should do */ endif;

//// for example:
if($theWeather == 'sunny') { 
	echo "the sun is shining";
} else {
	echo "no sun today :(";
}

//======================================================================
// FOREACH STATEMENTS
//======================================================================

//// a foreach statement goes through a function for each value inside an array:

$myArray = array('item 1', 'item 2', 'item 3');
foreach($myArray as $value ) {
	// what you want it to do, for example:
	echo "$value - ";
}
// this function will output: 'item 1 - item 2 - item 3"

// nesting foreach
// it is possible nest foreachs inside each other:
$myQuestions = array(
	
	'Question 1' => array('Answer 1', 'Answer 2', 'Answer 3'),
	'Question 2' => array('Answer 1', 'Answer 2', 'Answer 3'),
	'Question 3' => array('Answer 1', 'Answer 2', 'Answer 3')

);

foreach($myQuestions as $question => $answers) {
	echo "$question: ";
	
	foreach($answers as $answer) {
		echo "$answer, ";
	} 
	
	echo "<br />";
}

// this will output: 
//// Question 1: Answer 1, Answer 2, Answer 3,
//// Question 2: Answer 1, Answer 2, Answer 3,
//// Question 3: Answer 1, Answer 2, Answer 3,


// another nesting example:
$book = array(
	
	'Chapter 1' => array(
		
		'paragraph 1' => 'content',
		'paragraph 2' => 'content',
		'paragraph 3' => 'content',	
		
	),
	
	'Chapter 2' => array(
		
		'paragraph 1' => 'content',
		'paragraph 2' => 'content',
		'paragraph 3' => 'content',	
		
	)

);

foreach($book as $chapter => $paragraphs) {
	echo "<h2>$chapter</h2>";
	
	foreach($paragraphs as $paragraph => $content) {
		echo "$paragraph: $content<br /><br />";
	} 
	
	echo "<br /><br />";
}

// this will output:
//// Chapter 1
//// paragraph 1: content
//// paragraph 2: content
//// paragraph 3: content
//// paragraph 4: content

//// Chapter 2
//// paragraph 1: content
//// paragraph 2: content
//// paragraph 3: content
//// paragraph 4: content



//======================================================================
// FUNCTION EXAMPLES I STOLE FROM CHLOË
//======================================================================
// Load campaign questions
function loadQuestions() {
	include(V2_THEME_QUESTIONS);
}

// Check if constant is defined, echo it, if not; echo nothing
function getConstant($constant, $html_before = '', $html_after = '') {
	
	if(defined($constant)) {
        return $html_before . constant($constant) . $html_after;
    }

}

// Check if request CSS file is existing, if so; output HTML to load it (with PHP new line), if not; do nothing
function getCSS($file) {
    
    if(file_exists($file)) {
        echo "<link href=\"$file\" rel=\"stylesheet\" />\n";   
    } 
        
}


//======================================================================
// OTHER
//======================================================================

// retrieve parameter from url
$_GET['parameter'];


// include files
//// if folder is inside your current working folder:
include('path/file.php');
//// if folder is somewhere-else, always use document root:
include($_SERVER['DOCUMENT_ROOT'] . 'path/file.php');


// example of date settings
// see also: http://php.net/manual/en/function.date.php + settings on server should be enabled!
setlocale(LC_TIME, 'DE-de');
setlocale(LC_ALL, 'de_DE'); // language settings	
$date = strtotime("+1 months"); 
echo utf8_encode(strftime('%B',$date)); // date + 1 month, utf8 encoded
echo strftime('%d %B %Y',time()); // current date


// retrieve all files in a certain directory
scandir('your/path/here');

// retrieve filename without extentions
basename("your/path/here/file.php", ".php");

// check if a file exists
file_exists('your/file/here.php');




// close tag ?>

<!-- inside your html, you can echo a constant or variable directly like this: -->
<?= YOUR_CONSTANT; ?>
<?= $yourVar; ?>















